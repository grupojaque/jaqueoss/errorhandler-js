const i18n = new (require('i18n-2'))({
   locales: ['es'],
   directory: 'config/locales',
   extension: '.json',
   defaultLocale: 'es'
});
const finnerCodes = {
  'auth_header_missing': 4014,
  'missing_arguments': 4001,
  'required': 4000,
  'empty_string': 4221,
  'invalid_format': 4224,
  'only_letters': 4227,
  'invalid_phone': 4229,
  'exact_len %s': 4226,
  'max_len %s': 4223,
  'min_len %s': 4222,
  'min %s': 4222,
  'max %s': 4223,
  'foreign_key_missing': 4042,
  'already_exists': 4091,
  'string type_expected': 4002,
  'number type_expected': 4002,
  'array type_expected': 4002,
  'boolean type_expected': 4224,
  'json type_expected': 4002,
  'invalid_credentials': 4012,
  'not_found_Admin': 4041,
  'route_not_found': 4040
}
module.exports = {
  /***************************************************************************
  * An object with http status names as keys and the corresponding code as   *
  * value.                                                                   *
  * Defaults:                                                                *
  *                                                                          *
  * invalid: 422,                                                            *
  * conflict: 409,                                                           *
  * badRequest: 400,                                                         *
  * forbidden: 403,                                                          *
  * unauthorized: 401,                                                       *
  * unavailableForLegalReasons: 451,                                         *
  * failedDependency: 424,                                                   *
  * preconditionFailed: 412,                                                 *
  * unsupportedMediaType: 415,                                               *
  * notImplemented: 501,                                                     *
  * badGateway: 502,                                                         *
  * gone: 410,                                                               *
  * tooManyRequests: 429,                                                    *
  * notFound: 404,                                                           *
  * serverError: 500,                                                        *
  * redirect: 307,                                                           *
  * teapot: 418,                                                             *
  * notAcceptable: 406,                                                      *
  * serviceUnavailable: 503,                                                 *
  * locked: 423                                                              *
  ***************************************************************************/

  httpCodes:{
    newHTTP: 902
  },

  /***************************************************************************
  * Indicates the logger, or false for no logs                               *
  *                                                                          *
  * Default: console                                                         *
  ***************************************************************************/

  logging: console,

  /***************************************************************************
  * Function to define what you want to log before responding                *
  ***************************************************************************/
  beforeResponse: function(error, body, req){
    ErrorHandler.config.logging.error(`Sending ${error.status}: ` + body.message);
    ErrorHandler.config.logging.info(body);
  },
  /***************************************************************************
  * Function to customize the response body                                  *
  * In this example we use the i18n-2 module to transleate the message       *
  ***************************************************************************/
  customizeBody: function(error, body, req) {
    i18n.setLocale(req.language);

    if (Array.isArray(error.message)) {
      if (error.errorDetail) {
        ErrorHandler.config.logging.error('If sending an array as message no errorDetail must be sent');
      }
      error.errorDetail = error.message;
    }
    if (Array.isArray(error.errorDetail)) {
      body.message = error.errorDetail[0]; //Use I18n2 to translate
    } else {
      error.errorDetail = [error.message];
    }

    body.message = i18n.__.apply(i18n, error.errorDetail);
    body.code = finnerCodes[error.errorDetail[0]];
    if (!body.code) {
      ErrorHandler.config.logging.warn('No code found for ' + error.errorDetail[0]);
      body.code = error.statusNumber();
    }

    if (!body.errors) {
      return;
    }

    body.errors.forEach((fieldError) => {
      fieldError.message = typeof fieldError.message == 'string' ? [fieldError.message] : fieldError.message;
      fieldError.code = finnerCodes[fieldError.message[0]];
      if (!fieldError.code) {
        ErrorHandler.config.logging.warn('No code found for ' + fieldError.message[0]);
      }
      fieldError.message = i18n.__.apply(i18n, fieldError.message);
    });

  },
  /***************************************************************************
  * Object to map possible errors to an ErrorHandler construction.           *
  * The keys must be the name or code of the specific error.                 *
  ***************************************************************************/
  constructors: {
    //Sequelize
    SequelizeUniqueConstraintError: function (err, req){
      const fields = err.errors.map((e) => e.path);
      return new ErrorHandler('conflict', 'bad_fields', [
        {
          fields: fields,
          message: 'already_exists'
        }
      ]);
    },
    SequelizeEmptyResultError: function (err, req){
      return new ErrorHandler('notFound','not_found_generic');
    },
    SequelizeForeignKeyConstraintError: function (err, req){
      ErrorHandler.config.logging.error('ForeignKey missing: ' + err.index);
      return new ErrorHandler(options.status || 'notFound', 'foreign_key_missing');
    },
    SequelizeTimeoutError: function (err, req){
      ErrorHandler.config.logging.error('Database deadlock', err);
      return new ErrorHandler('locked', 'unexpected_error');
    },
    SequelizeDatabaseError: function (err, req){
      //TODO Move lock only if not optimisic
      ErrorHandler.config.logging.warn('Database error: ' + err.message);
      ErrorHandler.config.logging.warn(err.sql);
      if (err.original.code == 'ER_LOCK_DEADLOCK') {
        return new ErrorHandler('locked', 'optimistic_locking');
      }

      return new ErrorHandler('serverError', 'unexpected_error');
    },
    SequelizeValidationError: function (err, req){
      const errors = [];

      err.errors.forEach((e) => {
        if (e.type == 'notNull Violation') {
          errors.push({
            fields: [e.path],
            message: 'required'
          });
        } else if (e.validatorKey == 'len') {
          const exact = e.validatorArgs[0] == e.validatorArgs[1] ||
          e.validatorArgs.length < 2;
          const upTo = e.validatorArgs[0] == 0;

          if (exact) {
            errors.push({
              fields: [e.path],
              message: ['exact_len %s', e.validatorArgs[0]]
            });
          } else if (upTo) {
            errors.push({
              fields: [e.path],
              message: ['max_len %s', e.validatorArgs[1]]
            });
          } else {
            if (e.value.length < e.validatorArgs[0]) {
              errors.push({
                fields: [e.path],
                message: ['min_len %s', e.validatorArgs[0]]
              });
            }

            if (e.value.length > e.validatorArgs[1]) {
              errors.push({
                fields: [e.path],
                message: ['max_len %s', e.validatorArgs[1]]
              });
            }
          }
        } else if (e.validatorKey == 'isEmail') {
          errors.push({
            fields: [e.path],
            message: 'invalid_format'
          });
        } else if (e.validatorKey == 'is') {
          errors.push({
            fields: [e.path],
            message: e.message
          });
        } else if (e.validatorKey == 'isIn') {
          errors.push({
            fields: [e.path],
            message: ['isIn %s', e.validatorArgs[0].join(',')]
          });
        } else if (e.validatorKey == 'min') {
          errors.push({
            fields: [e.path],
            message: ['min %s', e.validatorArgs[0]]
          });
        } else if (e.validatorKey == 'max') {
          errors.push({
            fields: [e.path],
            message: ['max %s', e.validatorArgs[0]]
          });
        } else if (e.validatorKey == 'notEmpty') {
          errors.push({
            fields: [e.path],
            message: 'empty_string'
          });
        } else {
          sails.confg.errorhandler.logging.warn('Cant parse validation: ' + e.validatorKey);
          errors.push({
            fields: [e.path],
            message: 'invalidField'
          });
        }
      });

      return new ErrorHandler('invalid', 'bad_fields', errors);
    },
    SequelizeOptimisticLockError: function(err, req){
      return new ErrorHandler('conflict', 'optimistic_locking');
    }
  }
}
