module.exports.errorhandler = {
  /***************************************************************************
  * An object with http status names as keys and the corresponding code as   *
  * value.                                                                   *
  * Defaults:                                                                *
  *                                                                          *
  * invalid: 422,                                                            *
  * conflict: 409,                                                           *
  * badRequest: 400,                                                         *
  * forbidden: 403,                                                          *
  * unauthorized: 401,                                                       *
  * unavailableForLegalReasons: 451,                                         *
  * failedDependency: 424,                                                   *
  * preconditionFailed: 412,                                                 *
  * unsupportedMediaType: 415,                                               *
  * notImplemented: 501,                                                     *
  * badGateway: 502,                                                         *
  * gone: 410,                                                               *
  * tooManyRequests: 429,                                                    *
  * notFound: 404,                                                           *
  * serverError: 500,                                                        *
  * redirect: 307,                                                           *
  * teapot: 418,                                                             *
  * notAcceptable: 406,                                                      *
  * serviceUnavailable: 503,                                                 *
  * locked: 423                                                              *
  ***************************************************************************/

  // httpCodes: {
  // },

  /***************************************************************************
  * Indicates the logger, or false for no logs                               *
  *                                                                          *
  * Default: console                                                         *
  ***************************************************************************/

  //logging: false,

  /***************************************************************************
  * Function to define what you want to do before responding                *
  ***************************************************************************/

  // beforeResponse: function(error, body, req){
  // },

  /***************************************************************************
  * Function to customize the response body                                  *
  ***************************************************************************/

  // customizeBody: function(error, body, req){
  // },

  /***************************************************************************
  * Object to map possible errors to an ErrorHandler construction.           *
  * The keys must be the name or code of the specific error.                 *
  ***************************************************************************/

  // constructors: {
  // }

}
