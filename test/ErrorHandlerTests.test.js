const request = require('supertest');
const config = require('../errorhandlerConfig');
const ErrorHandlerWithConfig = require('../index')(config);
global.ErrorHandler = ErrorHandlerWithConfig;
function unexpectedResolve() {
  throw new Error('the promise was unexpectedly fulfilled');
}

let correctBody0, correctBody1;
describe('ErrorHandler using config', function() {
  before(() => {
    return factory.build("admin")
    .then((adminBuilt) => {
      correctBody0 = adminBuilt.dataValues;
      correctBody0.deactivated=false;
      return factory.build("admin")
    })
    .then((adminBuilt) => {
      correctBody1 = adminBuilt.dataValues;
      correctBody1.deactivated=false;
    });
  });

  describe('Controllers', function() {
    it('should return an ok status', function() {
      return request("http://localhost:3000/")
        .post('/admins')
        .send(correctBody0)
        .set('Accept', 'application/json')
        .expect(201)
        .then((response) => {
        });
    });
    it('should manage a unexpected server error and add cofigured body', function() {
      return request("http://localhost:3000/")
        .post('/admins/error')
        .send(correctBody0)
        .expect(500)
        .then((response) => {
          response.body.should.have.property('errors');
          response.body.should.have.property('message','unexpected_error');
          response.body.should.have.property('code',500);
          response.body.errors.should.have.length(0);
        });
    });
    it('should manage a route not found using cofigured body', function() {
      return request("http://localhost:3000/")
        .post('/admins/notFound')
        .send(correctBody0)
        .expect(404)
        .then((response) => {
          response.body.should.have.property('errors');
          response.body.should.have.property('message','route_not_found');
          response.body.should.have.property('code',4040);
          response.body.errors.should.have.length(0);
        });
    });
    it('should return error thrown with errors with cofigured body', function() {
      return request("http://localhost:3000/")
        .post('/admins/errorThrow')
        .send(correctBody0)
        .expect(400)
        .then((response) => {
          response.body.should.have.property('errors');
          response.body.should.have.property('message','bad_request');
          response.body.should.have.property('code',400);
          response.body.errors[0].should.have.property('fields');
          response.body.errors[0].fields.should.have.length(1);
          response.body.errors[0].fields[0].should.eql('name');
          response.body.errors[0].message.should.eql('required');
          response.body.errors[0].code.should.eql(4000);
        });
    });
  });
  describe('Functions', function() {
    describe('Create', function() {
      it('should create an errror handler without errors', function() {
        const error = new ErrorHandler('notFound','not_found');
        error.name.should.eql('ErrorHandler');
        error.status.should.eql('notFound');
        error.message.should.eql('not_found');
        error.errors.should.eql([]);
      });
      it('should create an errror handler with errors', function() {
        const error = new ErrorHandler('conflict','unique',[{fields:['email'],message:'unique'}]);
        error.name.should.eql('ErrorHandler');
        error.status.should.eql('conflict');
        error.message.should.eql('unique');
        error.errors.should.have.length(1);
        error.errors[0].should.have.property('fields',['email']);
        error.errors[0].should.have.property('message','unique');
      });
    });
    describe('statusNumber', function() {
      it('should get a status from configured map', function() {
        const error = new ErrorHandler('newHTTP','unique',[{fields:['email'],message:'unique'}]);
        error.statusNumber().should.eql(902);
      });
    });
    describe('generateBody', function() {
      it('should generate body with customBody', function() {
        const error = new ErrorHandler('invalid','invalid',[{fields:['phone'],message:'invalid_phone'}]);
        body = error.generateBody({language:'es'});
        body.should.have.property('message','invalid');
        body.should.have.property('code',422);
        body.should.have.property('errors');
        body.errors.should.have.length(1);
        body.errors[0].should.have.property('message','invalid_phone');
        body.errors[0].should.have.property('code',4229);
        body.errors[0].should.have.property('fields',['phone']);
      });
      it('should generate body with customBody using errorDetail', function() {
        const error = new ErrorHandler('invalid','invalid',null,['invalid_message']);
        body = error.generateBody({language:'en'});
        body.should.have.property('message','invalid_message');
        body.should.have.property('code',422);
      });
    });

    describe('toErrorHandler', function() {
      it('should map error SequelizeValidationError required', function() {
        const createRequired =  Object.assign({},correctBody0);
        delete createRequired.email;
        return Admin.create(createRequired)
        .then(unexpectedResolve)
        .catch((errorSequelize) => {
          const error = ErrorHandler.toErrorHandler(errorSequelize,{});
          error.should.have.property('name','ErrorHandler');
          error.should.have.property('status','invalid');
          error.should.have.property('message','bad_fields');
          error.errors.should.have.length(1);
          error.errors[0].should.have.property('fields',['email']);
          error.errors[0].should.have.property('message','required');
        })
      });
      it('should map error SequelizeValidationError', function() {
        const createRequired =  Object.assign({},correctBody0);
        delete createRequired.email;
        return Admin.create(createRequired)
        .then(unexpectedResolve)
        .catch((errorSequelize) => {
          const error = ErrorHandler.toErrorHandler(errorSequelize,{});
          error.should.have.property('name','ErrorHandler');
          error.should.have.property('status','invalid');
          error.should.have.property('message','bad_fields');
          error.errors.should.have.length(1);
          error.errors[0].should.have.property('fields',['email']);
        })
      });
      it('should map error SequelizeUniqueConstraintError', function() {
        return Admin.create(correctBody0)
        .then((admin) => {
          return Admin.create({
            email: admin.email,
            name: correctBody1.name,
            last_name: correctBody1.last_name,
            phone: correctBody1.phone,
            role: correctBody1.role,
            deactivated: correctBody1.deactivated
          })
        })
        .then(unexpectedResolve)
        .catch((errorSequelize) => {
          const error = ErrorHandler.toErrorHandler(errorSequelize,{});
          error.should.have.property('name','ErrorHandler');
          error.should.have.property('status','conflict');
          error.should.have.property('message','bad_fields');
          error.should.have.property('errors');
          error.errors.should.have.length(1);
          error.errors[0].should.have.property('fields',['email']);
        })
      });
      it('should map error SequelizeEmptyResultError', function() {
        return Admin.findOne({rejectOnEmpty:true})
        .then(unexpectedResolve)
        .catch((errorSequelize) => {
          const error = ErrorHandler.toErrorHandler(errorSequelize,{});
          error.should.have.property('name','ErrorHandler');
          error.should.have.property('status','notFound');
          error.should.have.property('message','not_found_generic');
          error.errors.should.have.length(0);
        })
      });
      it('should map error SequelizeForeignKeyConstraintError', function() {
        return Admin.findOne({rejectOnEmpty:true})
        .then(unexpectedResolve)
        .catch((errorSequelize) => {
          const error = ErrorHandler.toErrorHandler(errorSequelize,{});
          error.should.have.property('name','ErrorHandler');
          error.should.have.property('status','notFound');
          error.should.have.property('message','not_found_generic');
          error.errors.should.have.length(0);
        })
      });
    });
  });
});


describe('ErrorHandler using defaults', function() {
  before(()=>{
    const ErrorHandlerDefault = require('../index')({});
    global.ErrorHandler = ErrorHandlerDefault;
  });
  describe('Controllers', function() {
    it('should return an ok status', function() {
      return request("http://localhost:3000/")
        .post('/admins')
        .send(correctBody0)
        .set('Accept', 'application/json')
        .expect(201)
        .then((response) => {
        });
    });
    it('should manage a unexpected server error', function() {
      return request("http://localhost:3000/")
        .post('/admins/error')
        .send(correctBody0)
        .expect(500)
        .then((response) => {
          response.body.should.have.property('errors');
          response.body.should.have.property('message','unexpected_error');
          response.body.should.not.have.property('code',500);
          response.body.errors.should.have.length(0);
        });
    });
    it('should return error thrown with errors', function() {
      return request("http://localhost:3000/")
        .post('/admins/errorThrow')
        .send(correctBody0)
        .expect(400)
        .then((response) => {
          response.body.should.have.property('errors');
          response.body.should.have.property('message','bad_request');
          response.body.errors[0].should.have.property('fields');
          response.body.errors[0].fields.should.have.length(1);
          response.body.errors[0].fields[0].should.eql('name');
          response.body.errors[0].message.should.eql('required');
        });
    });
  });
  describe('Functions', function() {
    describe('Create', function() {
      it('should create an errror handler without errors', function() {
        const error = new ErrorHandler('notFound','not_found');
        error.name.should.eql('ErrorHandler');
        error.status.should.eql('notFound');
        error.message.should.eql('not_found');
        error.errors.should.eql([]);
      });
      it('should create an errror handler with errors', function() {
        const error = new ErrorHandler('conflict','unique',[{fields:['email'],message:'unique'}]);
        error.name.should.eql('ErrorHandler');
        error.status.should.eql('conflict');
        error.message.should.eql('unique');
        error.errors.should.have.length(1);
        error.errors[0].should.have.property('fields',['email']);
        error.errors[0].should.have.property('message','unique');
      });
    });
    describe('statusNumber', function() {
      it('should get a status from string', function() {
        const error = new ErrorHandler('conflict','unique',[{fields:['email'],message:'unique'}]);
        error.statusNumber().should.eql(409);
      });
      it('should get a status from number', function() {
        const error = new ErrorHandler(409,'unique',[{fields:['email'],message:'unique'}]);
        error.statusNumber().should.eql(409);
      });
      it('should get a status from configured map', function() {
        const error = new ErrorHandler(409,'unique',[{fields:['email'],message:'unique'}]);
        error.statusNumber().should.eql(409);
      });
    });
    describe('generateBody', function() {
      it('should generate body', function() {
        const error = new ErrorHandler('conflict','unique',[{fields:['email'],message:'unique'}]);
        body = error.generateBody();
        body.should.have.property('message','unique');
        body.should.have.property('errors');
        body.errors.should.have.length(1);
        body.errors[0].should.have.property('message','unique');
        body.errors[0].should.have.property('fields',['email']);
      });
      it('should generate body with customBody', function() {
        const error = new ErrorHandler('conflict','unique',[{fields:['email'],message:'unique'}]);
        body = error.generateBody();
        body.should.have.property('message','unique');
        body.should.have.property('errors');
        body.errors.should.have.length(1);
        body.errors[0].should.have.property('message','unique');
        body.errors[0].should.have.property('fields',['email']);
      });
    });
    describe('mapErrors', function() {
      it('should map errors', function() {
        const errorThrown = new Error();
        errorThrown.name = 'nameToMap';
        try{
          ErrorHandler.mapErrors({'nameToMap': new ErrorHandler('notFound','not_found')})(errorThrown);
        }catch(errorMaped){
          errorMaped.name.should.eql('ErrorHandler');
          errorMaped.message.should.eql('not_found');
        }
      });
      it('should map errors', function() {
        const errorThrown = new Error();
        errorThrown.name = 'nameToMap';
        try{
          ErrorHandler.mapErrors({'otherName': new ErrorHandler('notFound','not_found')})(errorThrown);
        }catch(errorMaped){
          errorMaped.name.should.eql('nameToMap');
        }
      });
    });
    describe('toErrorHandler', function() {
      it('should return TypeError for not a mapped error', function() {
        const errorThrown = new Error();
        errorThrown.name = 'nameNotInMap';
        try{
          ErrorHandler.toErrorHandler(errorThrown,{});
        }catch(errorMaped){
          errorMaped.name.should.eql('TypeError');
        }
      });
      it('should map errors in config', function() {
        const errorThrown = new Error();
        errorThrown.name = 'nameNotInMap';
        try{
          ErrorHandler.toErrorHandler(errorThrown,{});
        }catch(errorMaped){
          errorMaped.name.should.eql('TypeError');
        }
      });
    });
  });
});
