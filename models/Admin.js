/**
 * Admin.js
 *
 * @description :: Admin model
 */
module.exports = (sequelize, DataTypes) => {
  const Admin = sequelize.define('admin', {
    email: {
      type: DataTypes.STRING(100),
      allowNull: false,
      validate: {
        len: [10, 100],
        isEmail: true,
        notEmpty: true
      },
      unique:true
    },
    password: {
      type: DataTypes.STRING
    },
    name: {
      type: DataTypes.STRING(30),
      allowNull: false,
      validate: {
        len: [3, 30],
        is: {
          args: new RegExp('^[A-Za-zÁÉÍÓÚÜÑáéíóúüñ][A-Za-zÁÉÍÓÚÜÑáéíóúüñ\\. ]*$'),
          msg: 'only_letters'
        },
        notEmpty: true
      }
    },
    last_name: {
      type: DataTypes.STRING(30),
      allowNull: false,
      validate: {
        len: [3, 30],
        notEmpty: true
      }
    },
    role: {
      type: DataTypes.ENUM('customer', 'admin'),
      allowNull: false,
      validate: {
        isIn: [['customer', 'admin']]
      }
    }
  }, {
    underscored: true,
    timestamps: true,
    paranoid: true,
    version: true
  });

  return Admin;
};
